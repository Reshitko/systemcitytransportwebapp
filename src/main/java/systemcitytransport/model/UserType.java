package systemcitytransport.model;

public enum UserType {
    BASIC_USER,
    ADMIN
}
