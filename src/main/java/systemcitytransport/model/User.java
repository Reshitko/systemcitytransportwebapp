package systemcitytransport.model;

import java.math.BigDecimal;

public class User {

    public User(String login, String password, BigDecimal funds, UserType type) {
        this.login = login;
        this.password = password;
        this.funds = funds;
        this.type = type;
    }

    private String login;
    private String password;
    private BigDecimal funds;
    private UserType type;

    public String getPassword(){
        return password;
    }
}
