package systemcitytransport.model.exception;

public class WrongPasswordException extends RuntimeException {
    public WrongPasswordException() {

    }

    public WrongPasswordException(String msg) {
        super(msg);
    }
}
