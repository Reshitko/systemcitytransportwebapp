package systemcitytransport.model.exception;

public class WrongLoginException extends RuntimeException {
    public WrongLoginException() {

    }

    public WrongLoginException(String msg) {
        super(msg);
    }
}

