package systemcitytransport.service;

public interface UserService {

    public boolean attemptLogin(String login, String password);

}
