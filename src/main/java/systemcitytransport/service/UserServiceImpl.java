package systemcitytransport.service;

import systemcitytransport.access.UserDao;
import systemcitytransport.access.UserDaoImpl;
import systemcitytransport.model.User;
import systemcitytransport.model.exception.WrongLoginException;
import systemcitytransport.model.exception.WrongPasswordException;

import java.sql.SQLException;

public class UserServiceImpl implements UserService {

    private static final String PASSWORD_REGEX = "^[a-zA-z0-9_]+$";
    private static final String LOGIN_REGEX = "^[a-zA-z0-9_]+$";

    private static final int LOGIN_LENGTH_MAX = 20;
    private static final int PASSWORD_LENGTH_MAX = 20;

    private static UserServiceImpl instance = null;

    private UserServiceImpl() throws SQLException {
        userDao = UserDaoImpl.getInstance();
    }

    public static synchronized UserServiceImpl getInstance() throws SQLException {
        if(instance == null) {
            instance = new UserServiceImpl();
        }
        return instance;
    }

    private UserDao userDao;

    @Override
    public boolean attemptLogin(String login, String password) throws IllegalArgumentException {
        checkLogin(login);
        checkPassword(password);
        User user = userDao.getUser(login);
        if (user.getPassword().equals(password)) {
            return true;
        } else {
            return false;
        }
    }

    private static void checkLogin (String login) throws IllegalArgumentException  {
        if(login.length() >= LOGIN_LENGTH_MAX) {
            throw new IllegalArgumentException ("Login too long!");
        }
        if(!login.matches(LOGIN_REGEX)) {
            throw new IllegalArgumentException ("Wrong symbols in login!");
        }
        //return true;
    }

    private static void checkPassword(String password) throws IllegalArgumentException  {
        if(password.length() >= PASSWORD_LENGTH_MAX) {
            throw new IllegalArgumentException ("Password too long!");
        }
        if(!password.matches(PASSWORD_REGEX)) {
            throw new IllegalArgumentException ("Wrong symbols in password!");
        }
        //return true;
    }
}
