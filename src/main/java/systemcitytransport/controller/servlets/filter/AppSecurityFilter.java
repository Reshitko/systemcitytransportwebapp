package systemcitytransport.controller.servlets.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = "/app/*")
public class AppSecurityFilter  implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpSession session = req.getSession(false);
        if(session != null) {
            String userName = (String)session.getAttribute("userName");
            if(userName != null) {
                filterChain.doFilter(servletRequest,servletResponse);
            } else {
                throw new ServletException("You must be authorized");
            }
        } else {
            throw new ServletException("You must be authorized");
        }

    }
}
