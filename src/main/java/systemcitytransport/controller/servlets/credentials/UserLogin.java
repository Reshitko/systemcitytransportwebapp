package systemcitytransport.controller.servlets.credentials;

import systemcitytransport.controller.servlets.RedirectServlet;
import systemcitytransport.service.UserService;
import systemcitytransport.service.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name="Login", urlPatterns = "/login")
public class UserLogin extends RedirectServlet {

    public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String login = request.getParameter("user");
        String password = request.getParameter("password");
        PrintWriter writer = response.getWriter();
        if(login.equals("") || password.equals("")) {
            writer.println("login or password is empty");
        } else {
            boolean successLogin = false;
            UserService userService = null;
            try {
                userService = UserServiceImpl.getInstance();
                successLogin = userService.attemptLogin(login, password);
            } catch (SQLException | IllegalArgumentException e) {
                writer.println(e.getMessage());
                return;
            }
            if(successLogin) {
                HttpSession session = request.getSession(true);
                session.setAttribute("userName", request.getParameter("user"));
                response.sendRedirect(request.getContextPath() + "/index.jsp");
            } else {
                writer.println("incorrect password");
            }
        }
    }
}
