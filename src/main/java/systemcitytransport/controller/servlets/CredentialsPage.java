package systemcitytransport.controller.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name="CredentialsPage", urlPatterns = "/credentials")
public class CredentialsPage extends RedirectServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("login")!=null){
            this.forward("/login", request, response);
        } else if (request.getParameter("registration")!=null) {
            this.forward("/registration.html", request, response);
        }
    }
}
