package systemcitytransport.controller.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name="StartPage", urlPatterns = "/start")
public class StartPage extends RedirectServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if(session == null) {
            this.forward("/credentials.jsp", request, response);
        } else {
            if (session.getAttribute("userName") == null) {
                this.forward("/credentials.jsp", request, response);
            } else {
                this.forward("/index.jsp", request, response);
            }
//            String s = (String) session.getAttribute("userName");
//            PrintWriter writer = response.getWriter();
//
//            writer.println(s);
//            session.invalidate();
        }
    }
}
