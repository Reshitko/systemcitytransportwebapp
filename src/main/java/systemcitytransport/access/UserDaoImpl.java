package systemcitytransport.access;

import systemcitytransport.model.User;
import systemcitytransport.model.UserType;
import systemcitytransport.service.UserServiceImpl;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Locale;
import java.util.UUID;

public class UserDaoImpl implements UserDao{

    private static UserDaoImpl instance;
    private UserDaoImpl() throws SQLException {
        DriverManager.registerDriver(new org.h2.Driver());
    }

    public static synchronized UserDaoImpl getInstance() throws SQLException {
        if(instance == null) {
            instance = new UserDaoImpl();
        }
        return instance;
    }

    private static final String DB_CONNECTION_STRING = "jdbc:h2:~/test_auth";
    private static final String DB_USER = "admin";
    private static final String DB_PASSWORD = "";

    @Override
    public User getUser(String login) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        User resultUser = null;
        try {
            conn = DriverManager
                    .getConnection(DB_CONNECTION_STRING
                            , DB_USER
                            , DB_PASSWORD);
            stmt = conn.prepareStatement(
                    "SELECT * FROM User WHERE login='" + login + "' LIMIT 1");
            rs = stmt.executeQuery();
            while(rs.next()) {
                String userLogin = rs.getString(1);
                String userPassword = rs.getString(2);
                UserType userType = UserType.valueOf(rs.getString(3).toUpperCase(Locale.ROOT));
                BigDecimal userFunds = rs.getBigDecimal(4);
                resultUser = new User(userLogin, userPassword, userFunds, userType);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {if (rs != null) rs.close();} catch (Exception e){};
            try {if (stmt != null) stmt.close();} catch (Exception e){};
            try {if (conn != null) conn.close();} catch (Exception e){};
        }
        if (resultUser == null) {
            throw new IllegalArgumentException("User not found");
        }
        return resultUser;
    }
}
