package systemcitytransport.access;

import systemcitytransport.model.User;

public interface UserDao {
    public User getUser(String login);
}
